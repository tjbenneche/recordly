// Dependencies
var express = require('express')
var request = require('request')
var jsonwebtoken = require('jsonwebtoken')
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
const MongoClient

// Local Dependencies
var fxns = require('./utils/fxns.js')


// Local Variables
var app = express()
var router = express.Router()
var envr = require('./' + (process.env.NODE_ENV) + '.json')

// Middleware
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use('/v1', router)
